import sys
import tree_reader
from math import log,log1p,exp

def get_same(names1,tre):
    total = set(tre.lvsnms())
    for j in tre.iternodes():
        if set(j.lvsnms()) == names1 or total-set(j.lvsnms()) == names1:
            return True
    return False

def logsum(l):
    x = l[0]
    for i in l[1:]:
        try:
            x += log1p(exp(i-x))
        except:
            x += 0
    return x

class Bipart:
    def __init__(self):
        self.inset = None
        self.onset = None
        self.freq = None
        self.count = None
    def is_conflict(self,inbp):
        x = inbp.inset.intersection(self.inset)
        if len(x) > 0 and len(inbp.inset.intersection(self.onset)) > 0 \
            and len(self.inset.intersection(inbp.onset)) > 0 \
            and len(self.onset.intersection(inbp.onset)) > 0:
            return True
        return False
    def __str__(self):
        return " ".join(self.inset)+" | "+" ".join(self.onset)

def get_bps(taxaset,bps):
    bp = None
    for i in bps:
        if i.inset == taxaset:
            bp = i
            break
    return bp

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python "+sys.argv[0]+" tfile lfile bpfile"
        sys.exit(0)
    #bp file
    bp = []
    bpconflicts = {}
    bpf = open(sys.argv[3],"r")
    bpf.readline()
    bpf.readline()
    curbp = None
    bp_trees = {} #key is bp and value is list of trees
    for i in bpf:
        if "CLADE:" not in i and "TREES:" not in i and "\tFREQ:" not in i:
            continue
        if "CLADE:" in i:
            i = i.replace("CLADE:","")
            spls = i.split("\t")
            tb = spls[0].split("|")
            b = Bipart()
            b.inset = set(tb[0].strip().split())
            b.onset = set(tb[1].strip().split())
            curbp = b
            if "\tFREQ:" in i:
                curbp.freq = float(spls[2].strip())
                curbp.count = float(spls[6].strip())
                bp.append(curbp)
        elif "\tFREQ:" in i:
            spls = i.strip().split("\t")
            curbp.freq = float(spls[-1].strip())
            curbp.count = float(spls[5].strip())
            bp.append(curbp)
        elif "TREES:" in i:
            spls = i.strip().split("\t")
            bp_trees[curbp] = [int(j) for j in spls[1].split()]
    bpf.close()
    for i in bp:
        bpconflicts[i] = []
        for j in bp:
            if i == j:
                continue
            if i.is_conflict(j): #can get that from bp
                bpconflicts[i].append(j)
    #tree file
    treef = open(sys.argv[1],"r")
    trees = []
    for i in treef:
        tree = tree_reader.read_tree_string(i)
        trees.append(tree)
    treef.close()
    lks = {}
    lf = open(sys.argv[2],"r")
    ts = lf.readline() # this is the tree/site line
    nsites = int(ts.strip().split(" ")[-1])
    print nsites
    best = [-99999999999999 for i in range(nsites)]
    for i in lf:
        spls = i.strip().split("\t")
        spls2 = spls[1].split(" ")
        tn = int(spls[0].replace("tr",""))-1
        lks[tn] = [float(j) for j in spls2]
        for j in range(nsites):
            if lks[tn][j] > best[j]:
                best[j] = lks[tn][j]
    lf.close()
    
    #totallg = [logsum(lks.values()) for i in lks]
    #print "sum lnlikes",totallg

    #do the calc
    #numtreescomp = []
    x = 0
    count = 0
    # for not just doing the ml tree
    for i in range(len(trees)):
        for j in trees[i].iternodes(order="postorder"):
            if len(j.children) < 2 or j == trees[i]:
                continue
            count += 1
            #if count == 35:
            #    break
            b = get_bps(set(j.lvsnms()),bp)
            # calc with the max for each edge
            #mx = max([lks[k] for k in bp_trees[b]])
            outf = open("fl"+str(count)+".sc.txt","w")
            outf2 = open("fl"+str(count)+".bp.txt","w")
            outf3 = open("fl"+str(count)+".max.txt","w")
            mx = []
            maxes = []
            maxesb = []
            for l in range(nsites):
                val = max([lks[k][l] for k in bp_trees[b]])
                mx.append(str(val))
                maxes.append(val)
                maxesb.append(b)
            outf.write(",".join(mx)+"\n")
            outf2.write(str(b)+"\n")

            tset = set()
            #have to make sure that trees aren't double dipped
            trees_dontdoit = set()
            print b
            for m in bpconflicts[b]:
                print "\t",m
                mxn = []
                for l in range(nsites):
                    val = max([lks[k][l] for k in bp_trees[m]])
                    mxn.append(str(val))
                    if val > maxes[l]:
                        maxes[l] = val
                        maxesb[l] = m
                outf.write(",".join(mxn)+"\n")
                outf2.write(str(m)+"\n")
            for m,n in zip(maxes,maxesb):
                outf3.write(str(n)+" "+str(m)+"\n")
            outf.close()
            outf2.close()
            outf3.close()
        break #only the first tree/ ML tree
            
