use Data::Dumper;
open(allout, ">All.txt");
open(best, ">best_and_species.txt");
open(out, ">Diff.txt");
$count = 0;
@AA = ();
open(SSL,$ARGV[0])||die "No SSLL file\n";
while($line = <SSL>){

	chomp $line;
	@array = split " ", $line;
	push (@AA,[@array]);
	$sites = $#array;
}
#print "$sites\n";
#print Dumper(\@AA);
@sum_array = ();
$count = 1;
open(Model,$ARGV[1])||die "No model\n";
while($line = <Model>){

	chomp $line;
	#May Need to edit next line depending on model file
	$len = ($line =~ m/.*?= (.*)/)[0];
	($start,$stop) = split "-", $len, 2;
	#print "start: $start\tstop: $stop\n";
	$start -= 1;
	$stop -= 1;
	foreach $i (0..$sites){
		$sum_array[$i] = 0;	
		foreach $x ($start..$stop){
			#print "$start: $AA[$x][$i]\n";
			$sum_array[$i] += $AA[$x][$i];
		}
		print allout "$sum_array[$i]\t";
	}
	print allout "\n";
	$best = -99999999.999;
	$bp = 0;
	#print "$#sum_array\n";
	foreach $i (1..$#sum_array){
		
		if($sum_array[$i] > $best){

			$best = $sum_array[$i];
			$bp = $i;
		} 		
	}
	print "$count\tbp\_$bp\t$sum_array[0]\t$best\n";
	print best "$count\tbp\_$bp\t$sum_array[0]\t$best\n";
	$diff = $best - $sum_array[0];
	print out "bp_$bp $diff\n";
	undef @sum_array;
	@sum_array = ();	
	$count++;
}

